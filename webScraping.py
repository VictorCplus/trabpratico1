from datetime import datetime
import re
from urllib.request import urlopen
from bs4 import BeautifulSoup
from urllib.error import HTTPError
from urllib.error import URLError


#Trabalho de extração de dados onde foi feito um web scraping do site https://books.toscrape.com/ respeitando os seguintes requisitos:
# Usar expressões regulares;
# Usar tratamento de exceções;
# Criar alerta de preço. Simular alteração de preço em casos de usar site fictício;
# Salvar dados extraídos em arquivo;
# Site de e-commerce: https://books.toscrape.com/. Usar a categoria de livro (travel, mystery, historical fiction, etc);
# Utilizar funções do módulo BS4 úteis para desenvolver o trabalho.

try:
#  Categoria: Historical
    html = urlopen('https://books.toscrape.com/catalogue/category/books/historical_42/index.html')
except HTTPError as e:
    print(e)
except URLError as e:
    print(e)
except AttributeError as e:
    print(e)
else:
    print('Ok'+'\n')

soup = BeautifulSoup(html, 'html.parser')

# Regex para descobrir o valor dos livros
regexValor = re.compile("\£\d+\.\d{2}")

# "w" para reescrever,  "a" para acrescentar sem alterar o anterior, "r' para ler os elementos dentro do txt e
# "r+" para ler e escrever no arquivo txt.

with open('arquivo.txt', 'a') as arquivo:

# Recupera data atual do sistema e formata para por no txt
    hoje = datetime.now()
    hoje = hoje.strftime("%d/%m/%Y, %H:%M:%S")
    hoje = str(hoje)

    arquivo.write('              ____________________ Livros ____________________\n\nValores do dia: '+hoje+'\n\n')
    for tag_produto in soup.find_all('article', class_='product_pod'):
        # encontrar o título do livro
        tag_titulo = tag_produto.h3.a
        titulo = tag_titulo.text.strip()

        # encontrar o preço do livro
        tag_preco = tag_produto.find('p', class_='price_color')
        preco_texto = tag_preco.text.strip()
        preco = re.findall(regexValor, preco_texto)[0]

        # imprimir o nome do livro e o seu preço
        print(f"Titulo = {titulo}\nPreço = {preco}\n")
        arquivo.write(str('Titulo: '+titulo+'\n'+'Preço: '+preco+'\n\n'))


# Função para limpar o conteúdo dentro do arquivo txt
def limpaArquivo():
    with open('arquivo.txt', 'r+') as arquivo:
        arquivo.truncate(0)


#limpaArquivo()
